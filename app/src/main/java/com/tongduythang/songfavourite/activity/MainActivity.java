package com.tongduythang.songfavourite.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.tongduythang.songfavourite.R;
import com.tongduythang.songfavourite.adapter.SongCustomListViewAdapter;
import com.tongduythang.songfavourite.model.Song;
import com.tongduythang.songfavourite.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private Button buttonAdd;
    private Button buttonRemove;
    private SongCustomListViewAdapter adapter;
    private EditText editTextName;
    private EditText editTextAuthor;
    private List<Song> songList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        initDataDefault();

        initListView();

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Utils.isKyTuHoaDauTien(editTextName.getText().toString().trim())){
                    Toast.makeText(MainActivity.this, "Ký tự đầu tiên phải viết hoa!", Toast.LENGTH_LONG).show();
                    return;
                }
                songList.add(new Song(editTextName.getText().toString(),
                        R.mipmap.ic_launcher,
                        editTextAuthor.getText().toString(), false));
                adapter.notifyDataSetChanged();
            }
        });

        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                songList = adapter.getSongs();
                for(int i = 0; i < songList.size(); i++){
                    if(songList.get(i).isChecked()){
                        songList.remove(songList.get(i));
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void initListView() {
        adapter = new SongCustomListViewAdapter(this, songList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initDataDefault() {
        songList.add(new Song("Tình trong mưa", R.mipmap.ic_launcher, "Tống duy thắng", true));
        songList.add(new Song("Tình trong nắng", R.mipmap.ic_launcher, "Tống duy thắng", false));
        songList.add(new Song("Tình trong sấm", R.mipmap.ic_launcher, "Tống duy thắng", true));
        songList.add(new Song("Tình trong sét", R.mipmap.ic_launcher, "Tống duy thắng", false));
        songList.add(new Song("Tình trong bùn", R.mipmap.ic_launcher, "Tống duy thắng", true));
        songList.add(new Song("Tình trong đất", R.mipmap.ic_launcher, "Tống duy thắng", false));
        songList.add(new Song("Tình trong mây", R.mipmap.ic_launcher, "Tống duy thắng", false));
        songList.add(new Song("Tình trong nước", R.mipmap.ic_launcher, "Tống duy thắng", true));
        songList.add(new Song("Tình trong lửa", R.mipmap.ic_launcher, "Tống duy thắng", true));
    }

    private void initView() {
        editTextAuthor = findViewById(R.id.edt_author);
        editTextName = findViewById(R.id.edt_name);
        listView = findViewById(R.id.listView);
        buttonAdd = findViewById(R.id.btnAdd);
        buttonRemove = findViewById(R.id.btnRemove);
    }
}