package com.tongduythang.songfavourite.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tongduythang.songfavourite.R;
import com.tongduythang.songfavourite.model.Song;

import java.util.List;

public class SongCustomListViewAdapter extends ArrayAdapter<Song> {
    private Context context;
    private List<Song> songs;


    public SongCustomListViewAdapter(@NonNull Context context, List<Song> songList) {
        super(context, R.layout.item_song);
        this.context = context;
        this.songs = songList;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;

        // we call an if statement on our view that is passed in,
        // to see if it has been recycled or not. if it has been recycled
        // then it already exists and we do not need to call the inflater function
        // this saves us a HUGE amount of resources and processing
        if(row == null){
            row = LayoutInflater.from(context).inflate(R.layout.item_song, null);
        }

        Song song = songs.get(position);
        if(song != null){
            TextView tvName = row.findViewById(R.id.tv_name);
            CheckBox checkBox = row.findViewById(R.id.checkbox);
            tvName.setText(String.format("%s", song.getName()));

            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                song.setChecked(isChecked);
            });
        }

        return row;
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    public List<Song> getSongs(){
        return songs;
    }
}
