package com.tongduythang.songfavourite.model;

public class Song {
    private String name;
    private int icon;
    private String nameAuthor;
    private boolean isChecked;

    public Song(String name, int icon, String nameAuthor, boolean isChecked) {
        this.name = name;
        this.icon = icon;
        this.nameAuthor = nameAuthor;
        this.isChecked = isChecked;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getName() {
        return name;
    }

    public boolean isChecked() {
        return isChecked;
    }
}
